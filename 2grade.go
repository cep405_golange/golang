package main
import "fmt"

func main() {

	getGrade := func(score int) string {
		switch {
			case score >= 80: return "A"
			case score >= 75: return "B+"
			case score >= 70: return "B"
			case score >= 65: return "C+"
			case score >= 60: return "C"
			case score >= 55: return "D+"
			case score >= 50: return "D"
			case score < 50: return "F"
		}
		return "check score value"
	}

	a := [5][4]int{	{70, 80, 60, 90},
					{78, 71, 72, 79},
					{63, 68, 64, 67},
					{99, 98, 91, 97},
					{82, 85, 81, 78}}

	for i := 0; i < 10; i++ {
		sum := 0
		fmt.Println("            Student : ",i+1)			// แสดง รหัสนักเรียน แล้วขึ้นบรรทัดใหม่ ด้วยคำสั่ง Println
		for j := 0; j < 4; j++ {							// loop 4 วิขา
			fmt.Print("Subject : ",j+1)						// แสดง รหัสวิชา ด้วยคำสั่ง Print
			fmt.Print(", Score : ",a[i][j])					// แสดง คะแนนของวิชานั้น ด้วยคำสั่ง Print
			fmt.Println(", Grade : ",getGrade(a[i][j]))		// แสดง เกรดของวิชานั้น แล้วขึ้นบรรทัดใหม่ ด้วยคำสั่ง Println
			sum += a[i][j]									// รวม คะแนนทั้ง 4 วิชา
		}
		ave := sum/4										// หาค่าเกรดเฉี่ย
		fmt.Println("AVGrade : ",getGrade(ave)) 			// ใส่ค่า
		fmt.Println();
		if i == 4 {
			break
		}
	}
}