package main

import "fmt"

///  Recursion function Fact !  ///

func fact(n int) int {
    if n == 0 {
        return 1
    }
    return n * fact(n-1)  //เรียกใช้ ตัวเอง
}

func main() {
    fmt.Println(fact(5))
}