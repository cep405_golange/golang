package main

import "fmt"

func main() {
	i, j := 1, 9

	p := &i         // ตัวแปร p point ชี้ไปที่ i
	fmt.Println(*p) // แสดง *p จะได้ค่า i 
	*p = 2        // กำหนด *p = 2 เท่ากับว่า i = 2 ด้วย
	fmt.Println(i)  // แสดง i

	p = &j         // ตัวแปร p point ชี้ไปที่ j
	*p = *p / 3  // *p ชี้ที่ j แปลว่า j หาร 3 เก็บใน j
	fmt.Println(j) // แสดง คำตอบ 9/3 = 3
}